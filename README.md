# Power Delivery Unit (PDU) Gateway

The goal of the project is to create a gateway that can speak with as many PDU
models as possible without the need for configuration files.

The service exports a REST interface that:

 * Exposes the list of drivers supported / models
 * Registers PDUs
 * List the available ports of a PDU
 * Sets / Gets the state of of ports

## Supported PDUs

Any SNMP-enabled PDU is supported by this project, but the following models have
pre-baked configuration to make things easier:

 * APC's Masterswitch: `apc_masterswitch`
 * Cyberpower's PDU41004: `cyberpower_pdu41004` (probably working on all cyberpower PDUs)

See [Registering an SNMP-enabled PDU](#registering-an-snmp-enabled-pdu) for more
information on how to set up your PDU.

## Setting up a development tree

To be up and ready, please type the following commands:

    git clone https://gitlab.freedesktop.org/mupuf/pdu-gateway.git
    cd pdu-gateway
    docker buildx build -t registry.freedesktop.org/mupuf/pdu-gateway:latest . --load
    docker run -v $(pwd):/app --entrypoint sh --rm -it -p 127.0.0.1:5000:80/tcp registry.freedesktop.org/mupuf/pdu-gateway:latest
    ./pdugateway.py

To create a dummy PDU, you can use curl:

    curl http://127.0.0.1:5000/v1/pdus -X POST -H "Content-Type: application/json" -d '{"pdu_name": "MyPDU", "model": "dummy", "config":{ "ports": ["label 1", "label 2", "label 3"]} }'

You can then explore the list of available ports and their status:

    curl http://127.0.0.1:5000/v1/pdus/MyPDU/ports
    {"0":{"label":"label 1","state":"ON"},"1":{"label":"","state":"ON"},"2":{"label":"label 2","state":"ON"}}

To change the state of a port, you can use the following command:

    curl http://127.0.0.1:5000/v1/pdus/MyPDU/ports/0/state -X POST -H "Content-Type: application/json" -d '{"state": "OFF"}'
    OFF

The return value of the command is the new state the port is in, so if you see
the state you wanted, then the operation was a success :)

Enjoy!

## Gotchas

Be warned that the current REST interface is *not* stable just yet.

## Deploying the service in production

The service should be deployed in the following way:

    docker run --rm -p 127.0.0.1:5000:80/tcp registry.freedesktop.org/mupuf/pdu-gateway:latest

This will run the service and make it accessible to localhost on the port 5000.

If you have a list of static PDU mappings you'd like the service to
start up with, you can export the `PDUGATEWAY_STATIC_PDUS` environment
variable with the following JSON schema,

    PDUGATEWAY_STATIC_PDUS='[pdu_object_1, pdu_object_2, ..., pdu_object_n]'

For example, in the case of one static PDU,

    PDUGATEWAY_STATIC_PDUS='[{"pdu_name": "farm-pdu-1", "model": "cyberpower_pdu41004", "config": {"hostname": "10.42.0.2"}}]'

## Registering an SNMP-enabled PDU

### Already-supported PDUs

If your PDU is in the supported list, then you are in luck and the only
information needed from you will be the model name, and the hostname of the
device:

    curl http://127.0.0.1:5000/v1/pdus -X POST -H "Content-Type: application/json" -d '{"pdu_name": "MyPDU", "model": "<<model>>", "config":{ "hostname": "<<ip_address>>"}}'

### Other SNMP-enabled PDUs

If your PDU model is currently-unknown, you will need to use the default SNMP
driver which will require a lot more information from you. Here is an example
for the `apc_masterswitch`:

    curl http://127.0.0.1:5000/v1/pdus -X POST -H "Content-Type: application/json" -d '{"pdu_name": "MyPDU", "model": "snmp", "config":{ "hostname": "10.0.0.42", "oid_outlets_label_base": "iso.3.6.1.4.1.318.1.1.4.4.2.1.4", "oid_outlets_base": "iso.3.6.1.4.1.318.1.1.4.4.2.1.3", "community": "private", "action_to_snmp_value": { "ON": 1, "OFF": 2, "REBOOT": 3} } }'

To figure out which values you need to set, I suggest you use an MIB Browser to
find the relevant fields. I personally used qtmib (Qt4-based), as it was the
only one that is packaged on my distro and still managed to compile, but you
should feel free to use any browser that works for you. When you have your
browser open, connect to your PDU using its IP, and find the following fields:

 * `oid_outlets_label_base`: Directory that contains the list of labels.
   In the case of APC's masterswitch, the address is: `enterprises.apc.products.hardware.masterswitch.sPDUOutletControl.sPDUOutletControlTable.sPDUOutletControlEntry.sPDUOutletCtlName`
   which translate to `iso.3.6.1.4.1.318.1.1.4.4.2.1.4`.
 * `oid_outlets_base`: Directory that contains the methods to control the ports, with the index starting from 1.
   In the case of APC's masterswitch, the address is: `enterprises.apc.products.hardware.masterswitch.sPDUOutletControl.sPDUOutletControlTable.sPDUOutletControlEntry.sPDUOutletCtl`
   which translate to `iso.3.6.1.4.1.318.1.1.4.4.2.1.3`.

When you are done, you will need to instruct this driver the mapping between
the wanted state, and the integer value to write. So far, all of them have been
the following mapping:

 * `ON`: 1
 * `OFF`: 2
 * `REBOOT`: 3

Try these values, and change them accordingly!

Once you have collected all this information, feel free to
[open an issue](https://gitlab.freedesktop.org/mupuf/pdu-gateway/-/issues/new)
to ask us to add this information to the list of drivers. Make sure to include
the curl command line you used to register your PDU!

## Frequently Asked Questions

### Why not use pdudaemon?

We initially wanted to use [pdudaemon](https://github.com/pdudaemon/pdudaemon),
but since it does not allow reading back the state from the PDU, it isn't
possible to make sure that the communication with the PDU is working which
reduces the reliability and debugeability of the system.

Additionally, pdudaemon requires a configuration file, which is contrary to the
objective of the project to be as stateless as possible and leave configuration
outside of the project. The configuration could have been auto-generated
on-the-fly but since there is no way to check if the communication with the PDU
is working, it would make for a terrible interface for users.

Finally, most of the drivers in the project are using a telnet interface rather
than SNMP, which makes them brittle and stateful. See for
[yourself](https://github.com/pdudaemon/pdudaemon/blob/master/pdudaemon/drivers/apc7952.py#L65).
