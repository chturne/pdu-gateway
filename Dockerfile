FROM python:3.8-alpine

RUN set -ex \
        && apk add --no-cache net-snmp-tools net-snmp-libs
COPY . /app
WORKDIR /app
RUN set -ex \
        && apk add --no-cache --virtual .build-deps gcc libc-dev linux-headers python3-dev net-snmp-dev \
        && ( pip install uwsgi ; pip install --no-cache-dir -r requirements.txt ) \
        && apk del --no-network .build-deps
ENV PDUGATEWAY_PORT=80
CMD uwsgi --socket 0.0.0.0:${PDUGATEWAY_PORT} --protocol=http -w wsgi:app
