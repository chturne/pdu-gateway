#!/usr/bin/env python3

import flask
import json
import os
from pdu import PDU, PDUPort, PDUState
import traceback


app = flask.Flask(__name__)


@app.before_first_request
def populate_static_pdus():
    statics = json.loads(os.environ.get('PDUGATEWAY_STATIC_PDUS', '[]'))
    for entry in statics:
        if not all([k in entry for k in ['model', 'pdu_name', 'config']]):
            raise ValueError("static entry is missing one of 'model', 'pdu_name', or 'config'")
        PDU.register(model_name=entry.get('model'),
                     pdu_name=entry.get('pdu_name'),
                     config=entry.get('config'))


def get_pdu_or_fail(pdu_name):
    pdu = PDU.get_by_name(pdu_name)
    if pdu is None:
        raise ValueError(f"Unknown pdu name '{pdu_name}'")
    return pdu


def has_no_empty_params(rule):
    defaults = rule.defaults if rule.defaults is not None else ()
    arguments = rule.arguments if rule.arguments is not None else ()
    return len(defaults) >= len(arguments)


@app.errorhandler(ValueError)
def handle_valueError_exception(error):
    traceback.print_exc()
    response = flask.jsonify({"error": str(error)})
    response.status_code = 400
    return response


@app.route("/")
def site_map():
    links = []
    for rule in app.url_map.iter_rules():
        # Filter out rules we can't navigate to in a browser
        # and rules that require parameters
        if "GET" in rule.methods and has_no_empty_params(rule):
            url = flask.url_for(rule.endpoint, **(rule.defaults or {}))
            links.append(url)

    return {"routes": links}


@app.route('/pdu_gateway')
def pdu_gateway():
    return {
        "version": 1,
        "supported_pdus": list(PDU.supported_pdus().keys()),
    }


@app.route('/v1/pdus', methods=['GET'])
def pdus_list():
    return {
        "pdus": list(PDU.registered_pdus().keys())
    }


@app.route('/v1/pdus', methods=['DELETE'])
def delete_all_pdus():
    PDU.full_reset()
    return ("", 200)


@app.route('/v1/pdus', methods=['POST'])
def register_pdu():
    params = flask.request.json

    if params is None:
        raise ValueError("The request is not JSON-based")

    PDU.register(model_name=params.get('model'),
                 pdu_name=params.get('pdu_name'),
                 config=params.get('config'))
    return ("", 200)


# TODO: Allow deleting a single PDU


@app.route('/v1/pdus/<pdu_name>/ports')
def list_ports(pdu_name):
    pdu = get_pdu_or_fail(pdu_name)

    ports = {}
    for port in pdu.ports:
        ports[port.port_id] = {
            "state": port.state.name
        }
        if port.label is not None:
            ports[port.port_id]["label"] = port.label

    return ports


@app.route('/v1/pdus/<pdu_name>/ports/<port_id>/state', methods=['GET'])
def get_port_state(pdu_name, port_id):
    pdu = get_pdu_or_fail(pdu_name)
    return PDUPort(pdu, port_id).state.name + "\n"


@app.route('/v1/pdus/<pdu_name>/ports/<port_id>/state', methods=['POST'])
def set_port_state(pdu_name, port_id):
    pdu = get_pdu_or_fail(pdu_name)

    params = flask.request.json
    if params is None:
        raise ValueError("The request is not JSON-based")

    if 'state' not in params:
        return ("The request is missing the state that needs to be set", 400)

    state = getattr(PDUState, params['state'], None)
    if state is None or not state.is_valid_action:
        valid_choices = [s.name for s in PDUState.valid_actions()]
        return (f"The state '{params['state']}' is invalid: Should be in {valid_choices}\n", 400)

    port = PDUPort(pdu, port_id)
    port.set(state)

    return port.state.name + "\n"


# TODO: Add a way to label the ports in the PDU itself

if __name__ == '__main__':  # pragma: nocover
    app.run()
