import json
import pytest
from contextlib import nullcontext as does_not_raise
from pdugateway import app, get_pdu_or_fail, populate_static_pdus
from pdu import PDU
from unittest import mock


@pytest.fixture
def client():
    app.config['TESTING'] = True

    with app.test_client() as client:
        yield client


def test_routes(client):
    rv = client.get('/')
    assert set([r for r in rv.json.get('routes', {})]) == set(["/v1/pdus", "/pdu_gateway", "/"])


def test_pdu_gateway(client):
    rv = client.get('/pdu_gateway')
    ret = rv.json

    assert ret['version'] == 1
    assert "dummy" in ret['supported_pdus']


def test_get_pdu_or_fail__non_existing_pdu():
    with pytest.raises(ValueError):
        get_pdu_or_fail("invalid")


def test_non_json_post_request(client):
    for url in ['/v1/pdus', '/v1/pdus/MyPDU/ports/1/state']:
        ret = client.post(url, data="Hello")
        assert ret.status_code == 400


def test_full_integration_test(client):
    # Make sure all the state is gone
    client.delete('/v1/pdus')

    # Check that we do not have any PDUs
    ret = client.get('/v1/pdus').json
    assert ret['pdus'] == []

    # Add a PDU
    rv = client.post('/v1/pdus', json={
        "model": "dummy",
        "pdu_name": "MyPDU",
        "config": {
            "ports": ['P1', 'P2', 'P3']
        }
    })
    assert rv.status_code == 200

    # Check that listing the ports
    ret = client.get('/v1/pdus/MyPDU/ports').json
    assert ret == {
        '0': {'label': 'P1', 'state': 'ON'},
        '1': {'label': 'P2', 'state': 'ON'},
        '2': {'label': 'P3', 'state': 'ON'}
    }

    # Check that the PDU is now visible in the list of PDUs
    ret = client.get('/v1/pdus').json
    assert ret['pdus'] == ["MyPDU"]

    # Read the port
    ret = client.get('/v1/pdus/MyPDU/ports/1/state')
    assert ret.data == b'ON\n'

    # Change the port's state
    ret = client.post('/v1/pdus/MyPDU/ports/1/state', json={"state": "REBOOT"})
    assert ret.status_code == 200
    assert ret.data == b'REBOOT\n'

    # Try to set the state, but without specifying it
    ret = client.post('/v1/pdus/MyPDU/ports/1/state', json={})
    assert ret.status_code == 400

    # Try to set an invalid state
    ret = client.post('/v1/pdus/MyPDU/ports/1/state', json={"state": "INVALID"})
    assert ret.status_code == 400

    # Read back the state
    ret = client.get('/v1/pdus/MyPDU/ports/1/state')
    assert ret.data == b'REBOOT\n'

    # Check that the change got reflected to the list of ports
    ret = client.get('/v1/pdus/MyPDU/ports').json
    assert ret == {
        '0': {'label': 'P1', 'state': 'ON'},
        '1': {'label': 'P2', 'state': 'REBOOT'},
        '2': {'label': 'P3', 'state': 'ON'}
    }


@pytest.mark.parametrize(
    "static_spec,calls,expectation",
    [
        ("""[{"model": "dummy", "config":{"hostname": "10.42.0.2"}}]""",
         [],
         pytest.raises(ValueError)),
        ("""[{"pdu_name": "MyPDU", "model": "dummy"}]""",
         [],
         pytest.raises(ValueError)),
        ("""[{"pdu_name": "MyPDU", "config":{"hostname": "10.42.0.2"}}]""",
         [],
         pytest.raises(ValueError)),
        ("""[{"pdu_name": "MyPDU", "model": "dummy", "config":{"hostname": "10.42.0.2"}}]""",
         [
             mock.call(model_name="dummy",
                       pdu_name="MyPDU",
                       config={"hostname": "10.42.0.2"}),
         ],
         does_not_raise()),
        ("""[{"pdu_name": "MyPDU", "model": "dummy", "config":{"hostname": "10.42.0.2"}}, {"pdu_name": "MyPDU2",
            "model": "cyberpower_pdu41004", "config":{ "hostname": "10.42.0.3"}}]""",
         [
             mock.call(model_name="dummy",
                       pdu_name="MyPDU",
                       config={"hostname": "10.42.0.2"}),
             mock.call(model_name="cyberpower_pdu41004",
                       pdu_name="MyPDU2",
                       config={"hostname": "10.42.0.3"})
         ],
         does_not_raise()),
        ("",
         [],
         pytest.raises(json.decoder.JSONDecodeError)),
    ],
)
def test_populate_static_pdus(monkeypatch, static_spec, calls, expectation):
    m = mock.Mock()
    with expectation:
        monkeypatch.setenv("PDUGATEWAY_STATIC_PDUS",
                           static_spec)
        monkeypatch.setattr(PDU, "register", m)
        populate_static_pdus()
    if len(calls):
        m.assert_has_calls(calls)
    else:
        m.assert_not_called()
