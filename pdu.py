from enum import IntEnum


class PDUState(IntEnum):
    UNKNOWN = 0
    OFF = 1
    ON = 2
    REBOOT = 3

    @classmethod
    def valid_actions(cls):
        return [s for s in PDUState if s.value > 0]

    @property
    def is_valid_action(self):
        return self in self.valid_actions()


class PDUPort:
    def __init__(self, pdu, port_id, label=None):
        self.pdu = pdu
        self.port_id = port_id
        self.label = label

    def set(self, state):
        self.pdu.set_port_state(self.port_id, state)

    @property
    def state(self):
        return self.pdu.get_port_state(self.port_id)


class PDU:
    pdus = {}

    def __init__(self, name):
        self.name = name

        self.pdus[name] = self

    @property
    def ports(self):
        # NOTICE: Left for drivers to implement
        return []

    def set_port_state(self, port_id, state):
        # NOTICE: Left for drivers to implement
        return False

    def get_port_state(self, port_id):
        # NOTICE: Left for drivers to implement
        return PDUState.UNKNOWN

    def unregister(self):
        del self.pdus[self.name]

    @classmethod
    def supported_pdus(cls):
        from drivers.apc import ApcMasterswitchPDU
        from drivers.cyberpower import PDU41004
        from drivers.dummy import DummyPDU
        from drivers.snmp import SnmpPDU

        return {
            "apc_masterswitch": ApcMasterswitchPDU,
            "cyberpower_pdu41004": PDU41004,
            "dummy": DummyPDU,
            "snmp": SnmpPDU,
        }

    @classmethod
    def register(cls, model_name, pdu_name, config):
        Driver = cls.supported_pdus().get(model_name)

        if Driver is None:
            raise ValueError(f"Unknown model name '{model_name}'")

        return Driver(pdu_name, config)

    @classmethod
    def get_by_name(cls, name):
        return cls.pdus.get(name)

    @classmethod
    def registered_pdus(cls):
        return cls.pdus

    @classmethod
    def full_reset(cls):
        cls.pdus.clear()
